*** Settings ***
Documentation    Un premier test d'accès à la page de login
Library    SeleniumLibrary


*** Keywords ***
Connect to the world
    [Documentation]    Prérequis aux cas de test
    Open Browser    ${url}    ${browser}    service_log_path=${{os.path.devnull}}
    
    # Uncomment if needed
    #Maximize Browser Window

    Wait Until Element Is Visible    ${webE-mAppointment}
    Capturer la page

Procédure d'authentification
    [Documentation]    Nous gérons à la fois les informations de login, de mot de passe et le clic sur le bouton d'authentification
    Page Should Contain Element    ${input-uName}
    Input Text    ${input-uName}    ${text-input-uName}
    Input Text    ${input-pwd}    ${text-input-pwd}
    Capturer la page
    Click Button    ${btn-logIn}

Procédure de complétion du formulaire de prise de rendez-vous
    [Documentation]    Nous gérons ici tous les champs du formulaire de prise de rendez-vous
    Select From List By Label    ${webE-facility}    ${text-webE-facility}
    Click Element    ${input-hospitalReadmission}
    Click Element    ${input-medicaid}
    Input Text    ${input-calendar}    ${text-input-calendar}
    Input Text    ${textarea-comment-locator}    ${text-textarea-comment}
    Capturer la page
    Click Button    ${btn-bAppointment}

Procédure de fin de test
    [Documentation]    Fin de test avec capture de page et fermeture du butineur
    Capturer la page
    Close Browser

Capturer la page
    [Documentation]    Centraliser ici la manière de capturer des photos de l'éxecution
    Capture Page Screenshot    EMBED

*** Variables ***
# Variables de prérequis
${browser}    firefox
${url}    https://katalon-demo-cura.herokuapp.com

# Les objets
${btn-logIn}    id:btn-login
${btn-bAppointment}    id:btn-book-appointment

${input-uName}    id:txt-username
${input-pwd}    id:txt-password
# ${input-hautUname}    placeholder:Username
# ${input-hautPwd}    placeholder:Password
${input-hospitalReadmission}    id:chk_hospotal_readmission
${input-medicaid}    id:radio_program_medicaid
${input-calendar}    id:txt_visit_date

${textarea-comment-locator}    name:comment

${webE-mAppointment}    id:btn-make-appointment
${webE-facility}    id:combo_facility
${webE-menu}    id:menu-toggle
${webE-menu-home}    Home

# Variables de texte
${text-input-calendar}    02/07/2022
${text-input-uName}    John Doe
${text-input-pwd}    ThisIsNotAPassword
${text-textarea-comment}    kikoo c moi
${text-webE-facility}    Hongkong CURA Healthcare Center

*** Test Cases ***
Login page test case

    Connect to the world
    
    Click Element    ${webE-mAppointment}
    Capturer la page
    
    # ${temp-Uname}=    Get Text    ${input-hautPwd}
    # Input Text    ${input-uName}    ${temp-Uname}
    Procédure d'authentification

    Page Should Contain    Make Appointment
    Capturer la page

    Procédure de complétion du formulaire de prise de rendez-vous

    Page Should Contain    Appointment Confirmation
    Page Should Contain    ${text-webE-facility}
    Capturer la page

    Click Element    ${webE-menu}
    Capturer la page
    Click Link    ${webE-menu-home}
    
    Page Should Contain Element    ${webE-mAppointment}
    Capturer la page

    Procédure de fin de test


