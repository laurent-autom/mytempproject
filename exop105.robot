*** Settings ***
Documentation    Un premier test d'accès à la page de login
Resource    exop105.resource

*** Test Cases ***
Make an appointment PASSANT

    Given Connect to the world
    
    Click Element    ${webE-mAppointment}
    Capturer la page
    
    # ${temp-Uname}=    Get Text    ${input-hautPwd}
    # Input Text    ${input-uName}    ${temp-Uname}
    When Procédure d'authentification    John Doe     ThisIsNotAPassword

    Page Should Contain    Make Appointment
    Capturer la page

    Then Procédure de complétion du formulaire de prise de rendez-vous

    Page Should Contain    Appointment Confirmation
    Page Should Contain    ${text-webE-facility}
    Capturer la page

    Click Element    ${webE-menu}
    Capturer la page
    Click Link    ${webE-menu-home}
    
    Page Should Contain Element    ${webE-mAppointment}
    Capturer la page

    And Procédure de fin de test


Make an appointment NON PASSANT

    Given Connect to the world
    
    Click Element    ${webE-mAppointment}
    Capturer la page
    
    # ${temp-Uname}=    Get Text    ${input-hautPwd}
    # Input Text    ${input-uName}    ${temp-Uname}
    When Procédure d'authentification    John Wayne     CowboyBebop

    Page Should Contain    Make Appointment
    Capturer la page

    Then Procédure de complétion du formulaire de prise de rendez-vous

    Page Should Contain    Appointment Confirmation
    Page Should Contain    ${text-webE-facility}
    Capturer la page

    Click Element    ${webE-menu}
    Capturer la page
    Click Link    ${webE-menu-home}
    
    Page Should Contain Element    ${webE-mAppointment}
    Capturer la page

    And Procédure de fin de test