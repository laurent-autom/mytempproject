*** Settings ***
Documentation    Un premier test d'accès à la page de login
Library    SeleniumLibrary


*** Keywords ***
 
   

*** Variables ***
${browser}    firefox
${url}    https://katalon-demo-cura.herokuapp.com
${btn-logIn}    id:btn-login
${btn-bAppointment}    id:btn-book-appointment

${input-uName}    id:txt-username
${input-pwd}    id:txt-password
${input-hautUname}    placeholder:Username
${input-hautPwd}    placeholder:Password
${input-hospitalReadmission}    id:chk_hospotal_readmission
${input-medicaid}    id:radio_program_medicaid
${input-calendar}    id:txt_visit_date

${textarea-comment-locator}    name:comment
${textarea-comment-text}    kikoo c moi

${webE-mAppointment}    id:btn-make-appointment
${webE-facility}    id:combo_facility
${webE-menu}    id:menu-toggle     #class:btn btn-dark btn-lg toggle
${webE-menu-home}    Home

*** Test Cases ***
Login page test case

    # Launch chosen browser and connect to webapp
    Open Browser    ${url}    ${browser}    service_log_path=${{os.path.devnull}}
    
    # Uncomment if needed
    #Maximize Browser Window

    Wait Until Element Is Visible    ${webE-mAppointment}
    Capture Page Screenshot    EMBED
    
    Click Element    ${webE-mAppointment}
    Capture Page Screenshot    EMBED
    
    # ${temp-Uname}=    Get Text    ${input-hautPwd}
    # Input Text    ${input-uName}    ${temp-Uname}
    Page Should Contain Element    ${input-uName}
    Input Text    ${input-uName}    John Doe
    Input Text    ${input-pwd}    ThisIsNotAPassword
    Capture Page Screenshot    EMBED
    Click Button    ${btn-logIn}

    Page Should Contain    Make Appointment
    Capture Page Screenshot    EMBED
    Select From List By Label    ${webE-facility}    Hongkong CURA Healthcare Center
    Click Element    ${input-hospitalReadmission}
    Click Element    ${input-medicaid}
    Input Text    ${input-calendar}    02/07/2022
    Input Text    ${textarea-comment-locator}    ${textarea-comment-text}
    Capture Page Screenshot    EMBED
    Click Button    ${btn-bAppointment}

    Page Should Contain    Appointment Confirmation
    Page Should Contain    Hongkong CURA Healthcare Center
    Capture Page Screenshot    EMBED

    Click Element    ${webE-menu}
    Capture Page Screenshot
    Click Link    ${webE-menu-home}
    
    Page Should Contain Element    ${webE-mAppointment}
    Capture Page Screenshot    EMBED

    # Shutdown procedure
    # Sleep    5
    Close Browser


