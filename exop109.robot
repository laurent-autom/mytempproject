*** Settings ***
Documentation    Un premier test d'accès à la page de login
Resource    exop109.resource

*** Test Cases ***
Make an appointment Tokyo

    Given Connect to the world
    
    Click Element    ${webE-mAppointment}
    Capturer la page
    
    # ${temp-Uname}=    Get Text    ${input-hautPwd}
    # Input Text    ${input-uName}    ${temp-Uname}
    When Procédure d'authentification    John Doe     ThisIsNotAPassword

    Page Should Contain    Make Appointment
    Capturer la page

    Then Procédure de complétion du formulaire de prise de rendez-vous    radio_program_medicare

    Page Should Contain    Appointment Confirmation
    Page Should Contain    ${text-webE-facility}
    Capturer la page

    Click Element    ${webE-menu}
    Capturer la page
    Click Link    ${webE-menu-home}
    
    Page Should Contain Element    ${webE-mAppointment}
    Capturer la page

    And Procédure de fin de test

Make an appointment Hongkong

    Given Connect to the world
    
    Click Element    ${webE-mAppointment}
    Capturer la page
    
    # ${temp-Uname}=    Get Text    ${input-hautPwd}
    # Input Text    ${input-uName}    ${temp-Uname}
    When Procédure d'authentification    John Doe     ThisIsNotAPassword

    Page Should Contain    Make Appointment
    Capturer la page

    Then Procédure de complétion du formulaire de prise de rendez-vous    radio_program_medicaid

    Page Should Contain    Appointment Confirmation
    Page Should Contain    ${text-webE-facility}
    Capturer la page

    Click Element    ${webE-menu}
    Capturer la page
    Click Link    ${webE-menu-home}
    
    Page Should Contain Element    ${webE-mAppointment}
    Capturer la page

    And Procédure de fin de test

Make an appointment Seoul

    Given Connect to the world
    
    Click Element    ${webE-mAppointment}
    Capturer la page
    
    # ${temp-Uname}=    Get Text    ${input-hautPwd}
    # Input Text    ${input-uName}    ${temp-Uname}
    When Procédure d'authentification    John Doe     ThisIsNotAPassword

    Page Should Contain    Make Appointment
    Capturer la page

    Then Procédure de complétion du formulaire de prise de rendez-vous    radio_program_none

    Page Should Contain    Appointment Confirmation
    Page Should Contain    ${text-webE-facility}
    Capturer la page

    Click Element    ${webE-menu}
    Capturer la page
    Click Link    ${webE-menu-home}
    
    Page Should Contain Element    ${webE-mAppointment}
    Capturer la page

    And Procédure de fin de test