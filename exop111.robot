*** Settings ***
Documentation    Un premier test d'accès à la page de login
Resource    exop111.resource

*** Test Cases ***
Make an appointment Medicare
    [Tags]    run-once
    Given Connect to the world
    
    Click Element    ${webE-mAppointment}
    Capturer la page
    
    # ${temp-Uname}=    Get Text    ${input-hautPwd}
    # Input Text    ${input-uName}    ${temp-Uname}
    When Procédure d'authentification    ${JDD1.username}    ${JDD1.password}

    Page Should Contain    Make Appointment
    Capturer la page

    Then Procédure de complétion du formulaire de prise de rendez-vous    ${JDD1.HCP}    ${JDD1.verifyHCP}

    Capturer la page

    Click Element    ${webE-menu}
    Capturer la page
    Click Link    ${webE-menu-home}
    
    Page Should Contain Element    ${webE-mAppointment}
    Capturer la page

    And Procédure de fin de test

Make an appointment Medicaid

    Given Connect to the world
    
    Click Element    ${webE-mAppointment}
    Capturer la page
    
    # ${temp-Uname}=    Get Text    ${input-hautPwd}
    # Input Text    ${input-uName}    ${temp-Uname}
    When Procédure d'authentification    ${JDD2.username}    ${JDD2.password}

    Page Should Contain    Make Appointment
    Capturer la page

    Then Procédure de complétion du formulaire de prise de rendez-vous    ${JDD2.HCP}    ${JDD2.verifyHCP}

    Capturer la page

    Click Element    ${webE-menu}
    Capturer la page
    Click Link    ${webE-menu-home}
    
    Page Should Contain Element    ${webE-mAppointment}
    Capturer la page

    And Procédure de fin de test

Make an appointment None

    Given Connect to the world
    
    Click Element    ${webE-mAppointment}
    Capturer la page
    
    # ${temp-Uname}=    Get Text    ${input-hautPwd}
    # Input Text    ${input-uName}    ${temp-Uname}
    When Procédure d'authentification    ${JDD3.username}    ${JDD3.password}

    Page Should Contain    Make Appointment
    Capturer la page

    Then Procédure de complétion du formulaire de prise de rendez-vous      ${JDD3.HCP}    ${JDD3.verifyHCP}  

    Capturer la page

    Click Element    ${webE-menu}
    Capturer la page
    Click Link    ${webE-menu-home}
    
    Page Should Contain Element    ${webE-mAppointment}
    Capturer la page

    And Procédure de fin de test